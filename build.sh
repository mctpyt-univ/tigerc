#! /bin/sh

cd source

mosmllex TigerLex.lex >/dev/null
mosmlyac TigerGrammar.y
mosmlc -liberal -o tiger TigerAST.sml        \
                         TigerLineNumber.sml \
                         TigerPP.sml         \
                         TigerTips.sml       \
                         TigerTemp.sig       \
                         TigerTemp.sml       \
                         TigerTab.sig        \
                         TigerTab.sml        \
                         TigerSRes.sml       \
                         TigerEscap.sig      \
                         TigerEscap.sml      \
                         TigerSeman.sig      \
                         TigerSeman.sml      \
                         TigerGrammar.sig    \
                         TigerGrammar.sml    \
                         TigerLex.sml        \
                         TigerMain.sml       \
                         TopSort.sml

rm TigerLex.sml TigerGrammar.sml TigerGrammar.sig *ui *uo
mv tiger ..

cd ..
