%{
open TigerAST

fun P () = !TigerLineNumber.lineNumber

fun name (SimpleVar s) = s
  | name _ = raise Fail "Imposible que no sea SimpleVar!"

fun addDec (TypeDec [dt]) (TypeDec hdt :: t) =
      TypeDec (dt :: hdt) :: t
  | addDec (FunctionDec [dt]) (FunctionDec hdt :: t) =
      FunctionDec (dt :: hdt) :: t
  | addDec d1 dl =
      d1 :: dl

%}

%token EOF
%token TYPE ARRAY OF VAR FUNCTION
%token LET IN END IF THEN ELSE WHILE DO FOR TO BREAK
%token DOT COLON COLONEQ COMMA SCOLON EQ
%token LPAREN RPAREN LBRACK RBRACK LBRACE RBRACE
%token AMPER PIPE LT LE GT GE NOTEQ
%token PLUS MINUS TIMES DIV MINUS NIL
%token<int>    NUM
%token<string> LITERAL ID

%type<TigerAST.exp>        prog
%type<TigerAST.ty>         ty
%type<string>              id
%type<TigerAST.field>      tyfield
%type<TigerAST.field list> tyflds
%type<TigerAST.exp>        exp
%type<TigerAST.exp list>   explist
%type<(TigerAST.symbol*TigerAST.exp) list> rec_fields
%type<TigerAST.exp list>   args
%type<var>                 l_value
%type<TigerAST.dec>        dec
%type<TigerAST.dec>        vardec
%type<TigerAST.dec>        fundec
%type<TigerAST.dec list>   decs

%nonassoc THEN
%left     ELSE
%nonassoc DO
%nonassoc OF
%nonassoc COLONEQ
%left     PIPE
%left     AMPER
%nonassoc EQ LT LE GT GE NOTEQ
%left     PLUS MINUS
%left     TIMES DIV

%start prog

%%

prog : exp EOF { $1 }
     ;

exp : NUM                                { IntExp ($1, P ()) }
    | LPAREN RPAREN                      { UnitExp (P ()) }
    | NIL                                { NilExp (P ()) }
    | LITERAL                            { StringExp ($1, P ()) }
    | BREAK                              { BreakExp (P ()) }
    | l_value                            { VarExp ($1, P ()) }
    | l_value COLONEQ exp                { AssignExp ({var = $1, exp = $3},
                                                      P ()) }
    | LPAREN exp SCOLON explist RPAREN   { SeqExp ($2 :: $4, P ()) }
    | exp PIPE exp                       { IfExp ({test  = $1,
                                                   then' = IntExp (1, P ()),
                                                   else' = SOME $3},
                                                  P ()) }
    | exp AMPER exp                      { IfExp ({test  = $1,
                                                   then' = $3,
                                                   else' = SOME
                                                     (IntExp (0, P ()))},
                                                  P ()) }
    | exp EQ exp                         { OpExp ({left  = $1,
                                                   oper  = EqOp,
                                                   right = $3},
                                                  P ()) }
    | exp LT exp                         { OpExp ({left  = $1,
                                                   oper  = LtOp,
                                                   right = $3},
                                                  P ()) }
    | exp LE exp                         { OpExp ({left  = $1,
                                                   oper  = LeOp,
                                                   right = $3},
                                                  P ()) }
    | exp GT exp                         { OpExp ({left  = $1,
                                                   oper  = GtOp,
                                                   right = $3},
                                                  P ()) }
    | exp GE exp                         { OpExp ({left  = $1,
                                                   oper  = GeOp,
                                                   right = $3},
                                                  P ()) }
    | exp NOTEQ exp                      { OpExp ({left  = $1,
                                                   oper  = NeqOp,
                                                   right = $3},
                                                  P ()) }
    | exp PLUS exp                       { OpExp ({left  = $1,
                                                   oper  = PlusOp,
                                                   right = $3},
                                                  P ()) }
    | exp MINUS exp                      { OpExp ({left  = $1,
                                                   oper  = MinusOp,
                                                   right = $3},
                                                  P ()) }
    | exp TIMES exp                      { OpExp ({left  = $1,
                                                   oper  = TimesOp,
                                                   right = $3},
                                                  P ()) }
    | exp DIV exp                        { OpExp ({left  = $1,
                                                   oper  = DivideOp,
                                                   right = $3},
                                                  P ()) }
    | MINUS exp                          { OpExp ({left = IntExp (0, P ()),
                                                   oper = MinusOp,
                                                   right = $2},
                                                  P ()) }
    | LPAREN exp RPAREN                  { $2 }
    | id LPAREN args RPAREN              { CallExp ({func = $1, args = $3},
                                                    P ()) }
    | IF exp THEN exp                    { IfExp ({test  = $2,
                                                   then' = $4,
                                                   else' = NONE},
                                                  P ()) }
    | IF exp THEN exp ELSE exp           { IfExp ({test  = $2,
                                                   then' = $4,
                                                   else' = SOME $6},
                                                  P ()) }
    | WHILE exp DO exp                   { WhileExp ({test = $2, body = $4},
                                                     P ()) }
    | FOR id COLONEQ exp TO exp DO exp   { ForExp ({var    = $2,
                                                    escape = ref false,
                                                    lo     = $4,
                                                    hi     = $6,
                                                    body   = $8},
                                                   P ()) }
    | LET decs IN END                    { LetExp ({decs = $2,
                                                    body = UnitExp (P ())},
                                                   P ()) }
    | LET decs IN exp END                { LetExp ({decs = $2, body = $4},
                                                   P ()) }
    | LET decs IN exp SCOLON explist END { LetExp ({decs = $2,
                                                    body = SeqExp ($4 :: $6,
                                                                   P ())},
                                                   P ()) }
    | l_value LBRACK exp RBRACK OF exp   { ArrayExp ({typ  = name $1,
                                                      size = $3,
                                                      init = $6},
                                                     P ()) }
    | id LBRACE rec_fields RBRACE        { RecordExp ({fields = $3,
                                                       typ    = $1},
                                                      P ()) }
    ;

explist: exp SCOLON explist { $1 :: $3 }
    | exp                   { [$1] }
    ;

rec_fields : id EQ exp COMMA rec_fields { ($1, $3) :: $5 }
           | id EQ exp                  { [($1, $3)] }
           |                            { [] }
           ;

decs : dec decs { addDec $1 $2 }
    |           { [] }
    ;

dec : TYPE id EQ ty { TypeDec [({name = $2, ty = $4}, P ())] }
    | vardec        { $1 }
    | fundec        { $1 }
    ;

ty : id                   { NameTy $1 }
   | LBRACE tyflds RBRACE { RecordTy $2 }
   | ARRAY OF id          { ArrayTy $3 }
   ;

id : ID { $1 }
    ;

tyflds : tyfield COMMA tyflds { $1 :: $3 }
    | tyfield                 { [$1] }
    |                         { [] }
    ;

vardec : VAR id COLONEQ exp          { VarDec ({name   = $2,
                                                escape = ref false,
                                                typ    = NONE,
                                                init   = $4},
                                               P ()) }
       | VAR id COLON id COLONEQ exp { VarDec ({name   = $2,
                                                escape = ref false,
                                                typ    = SOME $4,
                                                init   = $6},
                                               P ()) }
       ;

fundec : FUNCTION id LPAREN tyflds RPAREN EQ exp
           { FunctionDec [({name   = $2,
                            params = $4,
                            result = NONE,
                            body   = $7},
                           P ())] }
       | FUNCTION id LPAREN tyflds RPAREN COLON id EQ exp
           { FunctionDec [({name   = $2,
                            params = $4,
                            result = SOME $7,
                            body   = $9},
                           P ())] }
       ;
tyfield : id COLON id { {escape = ref false, name = $1, typ = NameTy $3} }
        ;

args : exp COMMA args { $1 :: $3 }
     | exp            { [$1] }
     |                { [] }
     ;

l_value : id                        { SimpleVar $1 }
        | l_value DOT id            { FieldVar ($1, $3) }
        | l_value LBRACK exp RBRACK { SubscriptVar ($1, $3) }
        ;

%%
