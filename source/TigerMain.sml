open TigerLex
open TigerGrammar
open TigerEscap
open TigerSeman
open BasicIO Nonstdio

fun lexstream (is : instream) =
      Lexing.createLexer (fn b => fn n => buff_input is b 0 n)

fun errParsing lexbuf = (print ("Error en parsing!("
                                ^ makestring (!lineNumber)
                                ^ ")[" ^ Lexing.getLexeme lexbuf ^ "]\n");
                         raise Fail "fin!")

fun main args =
      let fun optionArg l s = (List.exists (fn x => x = s) l,
                               List.filter (fn x => x <> s) l)
          val (arbol,   l1) = optionArg args "-arbol"
          val (escapes, l2) = optionArg l1   "-escapes"
          val (ir,      l3) = optionArg l2   "-ir"
          val (canon,   l4) = optionArg l3   "-canon"
          val (code,    l5) = optionArg l4   "-code"
          val (flow,    l6) = optionArg l5   "-flow"
          val (inter,   l7) = optionArg l6   "-inter"
          val entrada =
                case l7
                of [n] => (open_in n
                           handle _ => raise Fail (n ^ " no existe!"))
                 | []  => std_in
                 | _   => raise Fail "opcio'n dsconocida!"
          val lexbuf = lexstream entrada
          val expr = prog Tok lexbuf handle _ => errParsing lexbuf
          val _ = findEscape expr
          val _ = if arbol then TigerPP.exprAst expr else ()
      in
          transProg expr;
          print "yes!!\n"
      end
      handle Fail s => print ("Fail: " ^ s ^ "\n")

val _ = main (CommandLine.arguments ())
