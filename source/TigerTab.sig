signature TigerTab =
  sig

    type (''k, 'v) table

    exception AlreadyExists of string
    exception NotExists
    exception NotExistsS of string

    val new          : unit -> (''k, 'v) table
    val fromTab      : (''k, 'v) table -> (''k, 'v) table
    val fromList     : (''k * 'v) list -> (''k, 'v) table
    val name         : ''k -> ''k
    val contains     : ''k -> (''k, 'v) table -> bool
    val insert       : ''k -> 'v -> (''k, 'v) table -> (''k, 'v) table
    val update       : ''k -> 'v -> (''k, 'v) table -> (''k, 'v) table
    val find         : ''k -> (''k, 'v) table -> 'v option
    val fetch        : ''k -> (''k, 'v) table -> 'v
    val mapValues    : ('a -> 'b) -> (''c, 'a) table -> (''c, 'b) table
    val map          : ('a -> ''c) -> ('b -> 'd) -> ('a, 'b) table
                       -> (''c, 'd) table
    val revMap       : ('a -> ''b) -> ('c -> 'd) -> ('a, 'c) table
                       -> (''b, 'd) table
    val insertList   : (''k, 'v) table -> (''k * 'v) list -> (''k, 'v) table
    val toList       : (''k, 'v) table -> (''k * 'v) list
    val filter       : ('v -> bool) -> (''k, 'v) table -> (''k, 'v) table
    val fetchPred    : ('v -> bool) -> (''k, 'v) table -> (''k * 'v)
    val keys         : (''k, 'v) table -> ''k list

  end
