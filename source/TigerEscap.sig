signature TigerEscap =
  sig

    val findEscape : TigerAST.exp -> unit

  end
