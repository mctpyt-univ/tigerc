structure TigerTab :> TigerTab =
  struct

    structure P = Polyhash

    type ('a, 'b) table = ('a, 'b) P.hash_table

    exception AlreadyExists of string
    exception NotExists
    exception NotExistsS of string

    fun new () = P.mkPolyTable (100, NotExists)

    and fromTab t = let val t' = new ()
                    in P.apply (fn x => P.insert t' x) t; t' end

    and fromList l = insertList (new ()) l

    and name x = x

    and contains s t = case P.peek t s of SOME _ => true
                                        | NONE   => false

    and insert s e t = let val t' = P.copy t
                       in P.peekInsert t' (s, e); t' end

    and update s e t = let val t' = P.copy t
                       in P.insert t' (s, e); t' end

    and find s t = P.peek t s

    and fetch s t = case find s t of SOME t => t
                                   | NONE   => raise NotExists

    and mapValues f t = P.map (fn (_, e) => f e) t

    and map f g t =
        let
            val l' = P.listItems t
            val t' = P.mkPolyTable (100, NotExists)
        in
            List.app (fn (k, e) => P.insert t' (k, e))
                (List.map (fn (k, e) => (f k, g e)) l');
            t'
        end

    and revMap f g t =
        let
            val l' = rev (P.listItems t)
            val t' = P.mkPolyTable (100, NotExists)
        in
            List.app (fn (k, e) => P.insert t' (k, e))
                (List.map (fn (k, e) => (f k, g e)) l');
            t'
        end

    and insertList t l =
        let val t' = P.copy t
        in List.app (fn (s, e) => P.insert t' (s, e)) l; t' end

    and toList t = P.listItems t

    and filter f t =
        let val l = P.listItems t
            val t' = P.mkPolyTable (100, NotExists)
        in
            List.app (fn (k, e) => P.insert t' (k,e))
                (List.filter (fn (a, b) => f b) l);
            t'
        end

    and fetchPred f t = let val l = P.listItems t
                        in hd (List.filter (fn (a, b) => f b) l) end

    and keys t = List.map (fn (x, y) => x) (P.listItems t)

  end
