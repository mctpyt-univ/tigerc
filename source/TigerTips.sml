structure TigerTips =
  struct

      type unique = unit ref
      datatype type' = TUnit
                     | TNil
                     | TInt
                     | TString
                     | TArray  of type' * unique
                     | TRecord of (string * type' * int) list * unique
                     | TFunc   of type' list * type'
                     | TType   of string * type' option ref

  end
