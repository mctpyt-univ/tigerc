structure TigerSeman :> TigerSeman =
  struct

    open TigerAST
    open TigerSRes

    type expty = {exp : unit, ty : type'}

    type venv = (string, EnvEntry) T.table
    type tenv = (string, type')    T.table

    val typeEnv : tenv = T.fromList
          [("int", TInt), ("string", TString)]

    val varEnv : venv = T.fromList
          [("print", Func {level   = mainLevel,
                           label   = "print",
                           formals = [TString],
                           result  = TUnit,
                           extern  = true}),
           ("flush", Func {level   = mainLevel,
                           label   = "flush",
                           formals = [],
                           result  = TUnit,
                           extern  = true}),
           ("getchar", Func {level   = mainLevel,
                             label   = "getstr",
                             formals = [],
                             result  = TString,
                             extern  = true}),
           ("ord", Func {level   = mainLevel,
                         label   = "ord",
                         formals = [TString],
                         result  = TInt,
                         extern  = true}),
           ("chr", Func {level   = mainLevel,
                         label   = "chr",
                         formals = [TInt],
                         result  = TString,
                         extern  = true}),
           ("size", Func {level   = mainLevel,
                          label   = "size",
                          formals = [TString],
                          result  = TInt,
                          extern  = true}),
           ("substring", Func {level   = mainLevel,
                               label   = "substring",
                               formals = [TString, TInt, TInt],
                               result  = TString,
                               extern  = true}),
           ("concat", Func {level   = mainLevel,
                            label   = "concat",
                            formals = [TString, TString],
                            result  = TString,
                            extern  = true}),
           ("not", Func {level   = mainLevel,
                         label   = "not",
                         formals = [TInt],
                         result  = TInt,
                         extern  = true}),
           ("exit", Func {level   = mainLevel,
                          label   = "exit",
                          formals = [TInt],
                          result  = TUnit,
                          extern  = true})]

    fun actualType (TType (s, ref (SOME t))) = actualType t
      | actualType t                         = t

    fun typesEqual (TRecord _) TNil = true
      | typesEqual TNil (TRecord _) = true
      | typesEqual (TRecord (_, u1)) (TRecord (_, u2)) = (u1 = u2)
      | typesEqual (TArray (_, u1)) (TArray (_, u2)) = (u1 = u2)
      | typesEqual (TType (_, r)) b =
          let val a = case !r of SOME t => t
                               | NONE   => raise Fail "No debería pasar! (1)"
          in typesEqual a b end
      | typesEqual a (TType (_, r)) =
          let val b = case !r of SOME t => t
                               | NONE   => raise Fail "No debería pasar! (2)"
          in typesEqual a b end
      | typesEqual a b = (a = b)

    fun transExp (venv, tenv) =
          let fun error (s, p) = raise Fail ("Error -- línea "
                                             ^ Int.toString p
                                             ^ ": " ^ s ^ "\n")
              fun trexp (VarExp v) = trvar v
                | trexp (UnitExp _) = {exp = (), ty = TUnit}
                | trexp (NilExp _) = {exp = (), ty = TNil}
                | trexp (IntExp (i, _)) = {exp = (), ty = TInt}
                | trexp (StringExp (s, _)) = {exp = (), ty = TString}
                | trexp (CallExp({func, args}, nl)) =
                    {exp = (), ty = TUnit} (* COMPLETE *)
                | trexp (OpExp({left, oper = EqOp, right}, nl)) =
                    let
                      val {exp = _, ty = tyl} = trexp left
                      val {exp = _, ty = tyr} = trexp right
                    in
                      if typesEqual tyl tyr
                         andalso not (tyl = TNil andalso tyr = TNil)
                         andalso tyl <> TUnit
                      then {exp = (), ty = TInt}
                      else error ("Tipos no comparables", nl)
                    end
                | trexp (OpExp ({left, oper = NeqOp, right}, nl)) =
                    let
                      val {exp = _, ty = tyl} = trexp left
                      val {exp = _, ty = tyr} = trexp right
                    in
                      if typesEqual tyl tyr
                         andalso not (tyl = TNil andalso tyr = TNil)
                         andalso tyl <> TUnit
                      then {exp = (), ty = TInt}
                      else error("Tipos no comparables", nl)
                    end
                | trexp (OpExp ({left, oper, right}, nl)) =
                    let
                      val {exp = _, ty = tyl} = trexp left
                      val {exp = _, ty = tyr} = trexp right
                    in
                      if typesEqual tyl tyr
                      then case oper
                           of PlusOp   => if actualType tyl = TInt
                                          then {exp = (), ty = TInt}
                                          else error ("Error de tipos", nl)
                            | MinusOp  => if actualType tyl = TInt
                                          then {exp = (), ty = TInt}
                                          else error ("Error de tipos", nl)
                            | TimesOp  => if actualType tyl = TInt
                                          then {exp = (), ty = TInt}
                                          else error ("Error de tipos", nl)
                            | DivideOp => if actualType tyl = TInt
                                          then {exp = (), ty = TInt}
                                          else error ("Error de tipos", nl)
                            | LtOp     => if actualType tyl = TInt
                                             orelse actualType tyl = TString
                                          then {exp = (), ty = TInt}
                                          else error ("Error de tipos", nl)
                            | LeOp     => if actualType tyl = TInt
                                             orelse actualType tyl = TString
                                          then {exp = (), ty = TInt}
                                          else error ("Error de tipos", nl)
                            | GtOp       => if actualType tyl = TInt
                                               orelse actualType tyl = TString
                                            then {exp = (), ty = TInt}
                                            else error ("Error de tipos", nl)
                            | GeOp       => if actualType tyl = TInt
                                               orelse actualType tyl = TString
                                            then {exp = (), ty = TInt}
                                            else error ("Error de tipos", nl)
                            | _          => raise Fail
                                                  "No debería pasar! (3)"
                      else error ("Error de tipos", nl)
                    end
                | trexp (RecordExp({fields, typ}, nl)) =
                    let
                      (* Traducir cada expresión de fields *)
                      val tfields = map (fn (sy, ex) => (sy, trexp ex))
                                        fields

                      (* Buscar el tipo *)
                      val (tyr, cs) =
                            case T.find typ tenv
                            of SOME t =>
                                 (case actualType t
                                  of TRecord (cs, u) =>
                                       (TRecord (cs, u), cs)
                                   | _ => error (typ ^ " no es de tipo "
                                                 ^ "record", nl))
                             | NONE => error ("Tipo inexistente ("
                                              ^ typ ^ ")", nl)

                      (* Verificar que cada campo esté en orden y tenga
                         una expresión del tipo que corresponde *)
                      fun verificar [] [] = ()
                        | verificar (c :: cs) [] =
                            error ("Faltan campos", nl)
                        | verificar [] (c :: cs) =
                            error ("Sobran campos", nl)
                        | verificar ((s, t, _) :: cs)
                                    ((sy, {exp, ty}) :: ds) =
                            if s <> sy
                            then error ("Error de campo", nl)
                            else if typesEqual ty t
                                 then verificar cs ds
                                 else error ("Error de tipo del "
                                             ^ "campo" ^ s, nl)
                      val _ = verificar cs tfields
                    in
                      {exp = (), ty = tyr}
                    end
                | trexp (SeqExp (s, nl)) =
                    let val lexti = map trexp s
                        val exprs = map (fn {exp, ty} => exp) lexti
                        val {exp, ty = tipo} = hd (rev lexti)
                    in {exp = (), ty = tipo} end
                | trexp (AssignExp ({var = SimpleVar s, exp}, nl)) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
                | trexp (AssignExp ({var, exp}, nl)) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
                | trexp (IfExp ({test, then', else' = SOME else'}, nl)) =
                    let
                      val {exp = testexp, ty = tytest} = trexp test
                      val {exp = thenexp, ty = tythen} = trexp then'
                      val {exp = elseexp, ty = tyelse} = trexp else'
                    in
                      if actualType tytest = TInt
                         andalso typesEqual tythen tyelse
                      then {exp = (), ty = tythen}
                      else error ("Error de tipos en if", nl)
                    end
                | trexp (IfExp ({test, then', else' = NONE}, nl)) =
                    let
                      val {exp = exptest, ty = tytest} = trexp test
                      val {exp = expthen, ty = tythen} = trexp then'
                    in
                      if actualType tytest = TInt
                         andalso tythen = TUnit
                      then {exp = (), ty = TUnit}
                      else error ("Error de tipos en if", nl)
                    end
                | trexp (WhileExp ({test, body}, nl)) =
                    let
                      val ttest = trexp test
                      val tbody = trexp body
                    in
                      if actualType (#ty ttest) = TInt
                         andalso #ty tbody = TUnit
                      then {exp = (), ty = TUnit}
                      else if actualType (#ty ttest) <> TInt
                           then error ("Error de tipo en la condición", nl)
                           else error ("El cuerpo de un while no puede "
                                       ^ "devolver un valor", nl)
                    end
                | trexp (ForExp ({var, escape, lo, hi, body}, nl)) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
                | trexp (LetExp ({decs, body}, _)) =
                    let
                      val (venv', tenv', _) =
                            List.foldl (fn (d, (v, t, _)) => trdec (v, t) d)
                                       (venv, tenv, []) decs
                      val {exp = expbody, ty = tybody} =
                            transExp (venv', tenv') body
                    in
                      {exp = (), ty = tybody}
                    end
                | trexp (BreakExp nl) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
                | trexp (ArrayExp ({typ, size, init}, nl)) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
              and trvar (SimpleVar s, nl) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
                | trvar (FieldVar (v, s), nl) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
                | trvar (SubscriptVar (v, e), nl) =
                    {exp = (), ty = TUnit}  (* COMPLETE *)
              and trdec (venv, tenv) (VarDec ({name,
                                               escape,
                                               typ = NONE,
                                               init}, pos)) =
                    (venv, tenv, [])  (* COMPLETE *)
                | trdec (venv, tenv) (VarDec ({name,
                                               escape,
                                               typ = SOME s,
                                               init}, pos)) =
                    (venv, tenv, [])  (* COMPLETE *)
                | trdec (venv, tenv) (FunctionDec fs) =
                    (venv, tenv, [])  (* COMPLETE *)
                | trdec (venv, tenv) (TypeDec ts) =
                    (venv, tenv, [])  (* COMPLETE *)
          in trexp end

    fun transProg ex =
          let val main =
                    LetExp ({decs = [FunctionDec [({name   = "_tigermain",
                                                    params = [],
                                                    result = NONE,
                                                    body   = ex}, 0)]],
                             body = UnitExp 0}, 0)
              val _ = transExp (varEnv, typeEnv) main
          in print "bien!\n" end

  end
