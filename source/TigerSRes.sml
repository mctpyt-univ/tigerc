structure TigerSRes =
  struct

    open TigerAST
    open TigerTips
    structure T = TigerTab

    datatype EnvEntry = VIntRO  (* read-only integer *)
                      | Var  of {ty : type'}
                      | Func of {level   : unit,
                                 label   : TigerTemp.label,
                                 formals : type' list,
                                 result  : type',
                                 extern  : bool}

    val mainLevel = ()

  end
