{
open TigerGrammar

val lineNumber = TigerLineNumber.lineNumber
val commentLevel = ref 0

val stringToInt = valOf o Int.fromString
fun inc r = r := !r + 1
fun dec r = (r := !r - 1; !r)
fun intToHexCtrl n = "\\x" ^ (if n <= 15 then "0" else "")
                       ^ Int.fmt StringCvt.HEX n
fun ctrl0 "\\n" = "\\x0a"
  | ctrl0 "\\t" = "\\x09"
  | ctrl0 _     = raise Fail "error interno 1 en lex!"
fun ctrl1 s = let val c = ord (hd (tl (tl (explode s))))
              in intToHexCtrl (c - ord #"@") end
fun ctrl2 s = let val n = ord (valOf (Char.fromString s))
              in intToHexCtrl n end

exception NotFound
val keywords = Polyhash.mkPolyTable (20, NotFound)
val _ = List.app (fn (k, v) => Polyhash.insert keywords (k, v))
      [("type",     TYPE),
       ("array",    ARRAY),
       ("of",       OF),
       ("var",      VAR),
       ("function", FUNCTION),
       ("let",      LET),
       ("in",       IN),
       ("end",      END),
       ("if",       IF),
       ("then",     THEN),
       ("else",     ELSE),
       ("while",    WHILE),
       ("do",       DO),
       ("for",      FOR),
       ("to",       TO),
       ("break",    BREAK),
       ("nil",      NIL)]
fun keywordOrId s = Polyhash.find keywords s handle NotFound => ID s
}


(* Aliases *)

let WS  = [` ``\t``\r``\^L`]          (* Whitespace *)
let LL  = [`a`-`z`]                   (* Lowercase letters *)
let L   = [`A`-`Z``a`-`z`]            (* Letters *)
let LDU = [`A`-`Z``a`-`z``0`-`9``_`]  (* Letters, decimal digits and
                                         underscore *)
let D   = [`0`-`9`]                   (* Decimal digits *)


(* Rules *)

rule Tok = parse "/*"       { inc commentLevel; Comment lexbuf }
               | eof        { EOF }
               | `\n`       { inc lineNumber; Tok lexbuf }
               | WS+        { Tok lexbuf }
               | `.`        { DOT }
               | `:`        { COLON }
               | ":="       { COLONEQ }
               | `,`        { COMMA }
               | `;`        { SCOLON }
               | `=`        { EQ }
               | `(`        { LPAREN }
               | `)`        { RPAREN }
               | `[`        { LBRACK }
               | `]`        { RBRACK }
               | `{`        { LBRACE }
               | `}`        { RBRACE }
               | `&`        { AMPER }
               | `|`        { PIPE }
               | `<`        { LT }
               | "<="       { LE }
               | `>`        { GT }
               | ">="       { GE }
               | "<>"       { NOTEQ }
               | `+`        { PLUS }
               | `-`        { MINUS }
               | `*`        { TIMES }
               | `/`        { DIV }
               | `"`        { LITERAL (literal lexbuf) }
               | D+         { NUM (stringToInt (getLexeme lexbuf)) }
               | LL+        { keywordOrId (getLexeme lexbuf) }
               | L LDU*     { ID (getLexeme lexbuf) }
               | _          { raise Fail ("lex:[" ^ getLexeme lexbuf ^"]!") }
and literal = parse eof      { raise Fail "string sin terminar! " }
            | `\n`           { raise Fail "NL en string!" }
            | `"`            { "" }
            | "\\"[`"``\\`]  { getLexeme lexbuf ^ literal lexbuf }
            | "\\"[`n``t`]   { ctrl0 (getLexeme lexbuf) ^ literal lexbuf }
            | "\\^"[`@`-`_`] { ctrl1 (getLexeme lexbuf) ^ literal lexbuf }
            | "\\"D D D      { ctrl2 (getLexeme lexbuf) ^ literal lexbuf }
            | "\\"           { literal1 lexbuf }
            | _              { let val s = getLexeme lexbuf
                               in
                                 if s > "\^_" then s ^ literal lexbuf
                                 else raise Fail "caracter ilegal!"
                               end }
and literal1 = parse eof  { raise Fail "string incompleta!" }
                   | `\n` { inc lineNumber; literal1 lexbuf }
                   | WS+  { literal1 lexbuf }
                   | "\\" { literal lexbuf }
                   | _    { raise Fail "\\\\ malformada!" }
and Comment = parse "*/" { (if dec commentLevel = 0 then Tok else Comment)
                           lexbuf }
                  | "/*" { inc commentLevel; Comment lexbuf }
                  | eof  { raise Fail "coment incompleto!" }
                  | `\n` { inc lineNumber; Comment lexbuf }
                  | _    { Comment lexbuf }
;
