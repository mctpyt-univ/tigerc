# Unix makefile for tigermain example

MOSML_PREFIX = /usr
MOSML_TOOLS  = camlrunm $(MOSML_PREFIX)/share/mosml/tools
MOSML_LEX    = $(MOSML_PREFIX)/bin/mosmllex
MOSML_YACC   = $(MOSML_PREFIX)/bin/mosmlyac -v

GCC    = gcc
CFLAGS = -g
MOSML_COMPILE = $(MOSML_PREFIX)/bin/mosmlc -c -liberal
MOSML_LINK    = $(MOSML_PREFIX)/bin/mosmlc

# Unix
REMOVE  = rm -f
MOVE    = mv
EXEFILE =

# DOS
#REMOVE  = del
#MOVE    = move
#EXEFILE = .exe

.SUFFIXES :
.SUFFIXES : .sig .sml .ui .uo
.PHONY : all clean depend

GRALOBJS = TigerAST.uo        \
           TigerGrammar.uo    \
           TigerLex.uo        \
           TigerMain.uo       \
           TigerLineNumber.uo \
           TigerPP.uo         \
           TigerEscap.uo      \
           TigerTab.uo        \
           TigerSeman.uo      \
           TigerTemp.uo       \
           TopSort.uo


all: tiger

clean:
	$(REMOVE) Makefile.bak
	$(REMOVE) TigerGrammar.output
	$(REMOVE) TigerGrammar.sig
	$(REMOVE) TigerGrammar.sml
	$(REMOVE) TigerLex.sml
	$(REMOVE) tiger
	$(REMOVE) *.ui
	$(REMOVE) *.uo
	$(REMOVE) errlist
	$(REMOVE) *.o

depend: source/TigerAST.sml        \
        source/TigerMain.sml       \
        source/TigerLineNumber.sml \
        source/TigerPP.sml         \
        TigerGrammar.sml           \
        TigerLex.sml
	$(REMOVE) Makefile.bak
	$(MOVE) Makefile Makefile.bak
	$(MOSML_TOOLS)/cutdeps <Makefile.bak >Makefile
	$(MOSML_TOOLS)/mosmldep >>Makefile


tiger: $(GRALOBJS) $(OBJSGEN)
	$(MOSML_LINK) -o tiger $(EXEFILE) TigerMain.uo

TigerGrammar.sml TigerGrammar.sig: source/TigerGrammar.y
	$(MOSML_YACC) $<

TigerLex.sml: source/TigerLex.lex
	$(MOSML_LEX) $<

%.ui: %.sig
	$(MOSML_COMPILE) $<

%.uo: %.sml
	$(MOSML_COMPILE) $<


### DO NOT DELETE THIS LINE
TigerPP.uo: TigerAST.uo
TigerEscap.ui: TigerAST.uo
TigerGrammar.ui: TigerAST.uo
TigerSeman.ui: TigerAST.uo
TigerEscap.uo: TigerEscap.ui \
               TigerTab.ui   \
               TigerAST.uo
TigerSeman.uo: TigerSeman.ui \
               TigerSRes.uo  \
               TigerTab.ui   \
               TigerAST.uo
TigerGrammar.uo: TigerGrammar.ui    \
                 TigerLineNumber.uo \
                 TigerAST.uo
TigerSRes.uo: TigerTab.ui  \
              TigerTips.uo \
              TigerTemp.ui \
              TigerAST.uo
TigerTab.uo: TigerTab.ui
TigerMain.uo: TigerSeman.ui   \
              TigerEscap.ui   \
              TigerGrammar.ui \
              TigerLex.uo     \
              TigerPP.uo
TigerTemp.uo: TigerTemp.ui
TigerLex.uo: TigerGrammar.ui \
             TigerLineNumber.uo
